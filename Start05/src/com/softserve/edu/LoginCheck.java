package com.softserve.edu;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LoginCheck {
	
	private WebDriver browser;
	private String loginName = "admin";
	private String password = "admin";
	List<WebElement> listElements;
	
	
	@BeforeTest
	public void beforeTest() {
		browser = new FirefoxDriver();
		browser.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}	

	@Test
	public void login(){		
		browser.get("http://registrator.herokuapp.com/");
		
		browser.findElement(By.id("login")).click();
		browser.findElement(By.id("login")).clear();
		browser.findElement(By.id("login")).sendKeys(loginName);
		
		browser.findElement(By.id("password")).click();
		browser.findElement(By.id("password")).clear();
		browser.findElement(By.id("password")).sendKeys(password);
		
		browser.findElement(By.cssSelector(".btn.btn-primary")).click();
		
		String loggedName = browser.findElements(By.cssSelector(".btn.btn-primary.btn-sm")).get(0).getText().trim();
		listElements = browser.findElements(By.cssSelector(".btn.btn-primary.btn-sm"));
		WebElement firsElement = listElements.get(0);
		loggedName = firsElement.getText();
		loggedName = loggedName.trim();
		Assert.assertEquals(loggedName, loginName);
	}
	
	@Test
	public void logout(){
		WebElement secondElement = listElements.get(1);
		secondElement.click();
		browser.findElement(By.xpath("//a[@href='/logout']")).click();
		Assert.assertTrue(browser.getTitle().contains("Login"));
	}
	
	@AfterTest
	public void afterTest(){
		browser.close();
	}

}
