package com.softserve.edu;

import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class L10nCheck {
	
	private WebDriver browser;	
	private HashMap<String, String> lng = new HashMap<String, String>();
	
	
	@BeforeTest
	public void beforeTest() {
		browser = new FirefoxDriver();
		browser.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		lng.put("en","Forgot the password?");
		lng.put("ru", "������ ������?");
		lng.put("uk", "������ ������?");
		
	}
	
	@Test
	public void laungCheck(){
		browser.get("http://registrator.herokuapp.com/");
		for(Map.Entry<String, String> entry : lng.entrySet()){			
			Select dropdown = new Select(browser.findElement(By.id("changeLanguage")));
			dropdown.selectByValue(entry.getKey());
			String actualText = browser.findElement(By.className("forgot-password")).getText();
			Assert.assertEquals(entry.getValue(), actualText.trim());
		}		
	}
	
	@AfterTest
	public void afterTest(){
		browser.close();
	}

}
